﻿using Model;
using IDAL;
using SqlHelper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class DALCompany : ICompany, IDisposable
    {
        public Company Model = new Company();
        public ConnectionManager SqlConnection = new ConnectionManager();

        /// <summary>
        /// 添加公司账户
        /// </summary>
        /// <returns></returns>
        public bool Add()
        {
            if (!String.IsNullOrWhiteSpace(Model.Description) && !String.IsNullOrWhiteSpace(Model.Name) && !String.IsNullOrWhiteSpace(Model.Password) && !String.IsNullOrWhiteSpace(Model.Site) && !String.IsNullOrWhiteSpace(Model.UserName))
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("INSERT INTO Company(Name,Site,Contact,Email,UserName,Password,Description) VALUES(@CName,@CSite,@CContact,@CEmail,@CUserName,@CPassword,@CDescription)");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("@CName", Model.Name));
                Command.Parameters.Add(new SqlParameter("@CSite", Model.Site));
                Command.Parameters.Add(new SqlParameter("@CContact", Model.Contact));
                Command.Parameters.Add(new SqlParameter("@CEmail", Model.Email));
                Command.Parameters.Add(new SqlParameter("@CUserName", Model.UserName));
                Command.Parameters.Add(new SqlParameter("@CPassword", Model.Password));
                Command.Parameters.Add(new SqlParameter("@CDescription", Model.Description));
                return (SqlConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// 删除公司信息
        /// </summary>
        /// <returns></returns>
        public bool Delete()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("DELETE FROM Company WHERE Id = @CompanyId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("@CompanyId", Model.Id));
                return (SqlConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// 更改公司信息
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("UPDATE Company SET Name=@CName,Site=@CSite,Contact=@CContact,Email=@CEmail,UserName=@CUserName,Password=@CPassword,Description=@CDescription WHERE Id=@CompanyId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("@CName", Model.Name));
                Command.Parameters.Add(new SqlParameter("@CSite", Model.Site));
                Command.Parameters.Add(new SqlParameter("@CContact", Model.Contact));
                Command.Parameters.Add(new SqlParameter("@CEmail", Model.Email));
                Command.Parameters.Add(new SqlParameter("@CUserName", Model.UserName));
                Command.Parameters.Add(new SqlParameter("@CPassword", Model.Password));
                Command.Parameters.Add(new SqlParameter("@CompanyId", Model.Id));
                Command.Parameters.Add(new SqlParameter("@CDescription", Model.Description));
                return (SqlConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }

        }
        public void Dispose()
        {
            //throw new NotImplementedException();
        }




        /// <summary>
        /// 获取公司信息
        /// </summary>
        /// <param name="Where"></param>
        /// <returns></returns>
        public DataTable GetList(string Where)
        {
            StringBuilder CommandText = new StringBuilder();
            CommandText.Append("SELECT Id,Name,Site,Contact,Email,UserName,Password,Description FROM Company");
            if (Where != "")
            {
                CommandText.Append(" WHERE " + Where);
            }
            SqlCommand Command = new SqlCommand();
            Command.CommandText = CommandText.ToString();
            return SqlConnection.ExcuteDataTable(Command);
        }
    }
}
