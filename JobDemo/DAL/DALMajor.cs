﻿using IDAL;
using Model;
using SqlHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALMajor : IMajor, IDisposable
    {
        public Major Model = new Major();

        public ConnectionManager SQLConnection = new ConnectionManager();

        public bool Add()
        {
            if (!String.IsNullOrWhiteSpace(Model.Name))
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("INSERT INTO Major (Name) VALUES (@MajorName)");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("MajorName", Model.Name));
                return (SQLConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }

        public bool Delete()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("DELETE FROM Major WHERE Id = @MajorId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("MajorId", Model.Id));
                return (SQLConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }

        public bool Update()
        {
            if (Model.Id != 0)
            {
                StringBuilder CommandText = new StringBuilder();
                CommandText.Append("UPDATE Major SET Name = @MajorName WHERE Id = @MajorId");
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText.ToString();
                Command.Parameters.Add(new SqlParameter("MajorName", Model.Name));
                Command.Parameters.Add(new SqlParameter("MajorId", Model.Id));
                return (SQLConnection.ExcuteNonQuery(Command));
            }
            else
            {
                return false;
            }
        }

        public DataTable GetList(string Where)
        {
            StringBuilder CommandText = new StringBuilder();
            CommandText.Append("SELECT Id,Name FROM Major");
            if (Where != "")
            {
                CommandText.Append(" WHERE " + Where);
            }
            SqlCommand Command = new SqlCommand();
            Command.CommandText = CommandText.ToString();
            return SQLConnection.ExcuteDataTable(Command);
        }

        public bool MultiDel(string[] MajorIds)
        {
            SqlCommand[] Commands = new SqlCommand[MajorIds.Length];
            string CommandText = "DELETE FROM Major WHERE Id = ";
            for (int i = 0; i < MajorIds.Length; i++)
            {
                SqlCommand Command = new SqlCommand();
                Command.CommandText = CommandText+MajorIds[i];
                Commands[i] = Command;
            }
            return SQLConnection.ExcuteNonQuery(Commands);
        }

        public void Dispose()
        {

        }
    }
}
