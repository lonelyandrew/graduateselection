﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Admin : IDisposable
    {
        /// <summary>
        /// 管理员Id
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// 管理员用户名
        /// </summary>
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// 管理员密码
        /// </summary>
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// 管理员登陆错误判断
        /// </summary>
        public enum JudgeAdminLog
        {
            Success, UserNameNotExist, WrongPassword
        }




        public void Dispose()
        {

        }
    }
}
