﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using SqlHelper;

namespace JobDemo
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            ConnectionPoolManager ApplicationPoolManager = new ConnectionPoolManager(20);
            GlobalObj.GlobalObj.ApplicationStorage.Add("ApplicationPoolManager", ApplicationPoolManager);
        }
    }
}