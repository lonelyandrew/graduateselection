﻿using System;
using Tools;
using BLL;
using System.Data;

namespace JobDemo.Company
{
    public partial class CompanyRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Id"] != null)
                {
                    BindData();
                }
            }
        }

        private void BindData()
        {
            string StringSession = Session["Id"].ToString();
            DataRow Row = BLLCompany.GetCompInfo(StringSession);
            if (Row != null)
            {
                AddCompName.Value = Row["Name"].ToString().Trim();
                AddCompUsrName.Value = Row["UserName"].ToString().Trim();
                AddCompContact.Value = Row["Contact"].ToString().Trim();
                AddCompSite.Value = Row["Site"].ToString().Trim();
                AddCompEmail.Value = Row["Email"].ToString().Trim();
                ContentSpace.Value = Row["Description"].ToString();
            }
        }

        protected void CompRegSubmitEvent(object sender, EventArgs e)
        {
            DataRow Row = null;
            if (String.IsNullOrWhiteSpace(AddCompName.Value))
            {
                ScriptHelper.Alert("公司名字不能为空，请重新输入！", this.Page);
                return;
            }

            if (AddCompName.Value.Length > 16)
            {
                ScriptHelper.Alert("公司名字长度超出范围，请重新输入！", this.Page);
                return;
            }
            if (Session["Id"] == null && BLLCompany.ContainsCompUserName(AddCompName.Value))
            {
                ScriptHelper.Alert("公司名已存在，请重新输入！", this.Page);
                return;
            }
            else if (Session["Id"] != null)
            {
                using (Model.Company model = new Model.Company())
                {
                    model.Id = Int32.Parse(Session["Id"].ToString());
                    Row = BLLCompany.GetInfoById(model).Rows[0];
                    if (Row["Name"].ToString() != AddCompName.Value && BLLCompany.ContainsCompName(AddCompName.Value))
                    {
                        ScriptHelper.Alert("公司名已存在，请重新输入！", this.Page);
                        return;
                    }
                }
            }

            if (Session["Id"] == null && BLLCompany.ContainsCompUserName(AddCompUsrName.Value))
            {
                ScriptHelper.Alert("此用户名已存在，请重新输入！", this.Page);
                return;
            }
            else if (Session["Id"] != null)
            {
                if (Row["UserName"].ToString() != AddCompUsrName.Value && BLLCompany.ContainsCompUserName(AddCompUsrName.Value))
                {
                    ScriptHelper.Alert("此用户名已存在，请重新输入！", this.Page);
                    return;
                }
            }

            if (String.IsNullOrWhiteSpace(AddCompSite.Value))
            {
                ScriptHelper.Alert("公司地址不能为空！", this.Page);
                return;
            }
            if (string.IsNullOrWhiteSpace(AddCompUsrName.Value))
            {
                ScriptHelper.Alert("公司名字不可以为空，请重新输入！", this.Page);
                return;
            }
            if (String.IsNullOrWhiteSpace(AddCompPsd.Value))
            {
                ScriptHelper.Alert("密码不可为空！", this.Page);
                return;
            }
            if (String.IsNullOrWhiteSpace(AddCompRePsd.Value))
            {
                ScriptHelper.Alert("密码不能为空！", this.Page);
                return;

            }
            if (AddCompPsd.Value != AddCompRePsd.Value)
            {
                ScriptHelper.Alert("密码不一致，请重新输入！", this.Page);
                return;
            }

            Model.Company Model = new Model.Company();
            Model.Name = AddCompName.Value.Trim();
            Model.Contact = AddCompContact.Value.Trim();
            Model.Email = AddCompEmail.Value.Trim();
            Model.UserName = AddCompUsrName.Value.Trim();
            Model.Password = AddCompPsd.Value.Trim();
            Model.Site = AddCompSite.Value.Trim();
            Model.Description = Request["content"].ToString();

            if (Session["Id"] == null)
            {
                if (BLLCompany.AddCompany(Model))
                {
                    Response.Redirect("CompanyLogIn.aspx");
                }
                else
                {
                    ScriptHelper.Alert("注册失败，请联系管理员。", this.Page);
                    return;
                }
            }
            else
            {
                Model.Id = Int32.Parse(Session["Id"].ToString());
                if (BLLCompany.UpdateCompany(Model))
                {
                    Response.Redirect("CompanyLogIn.aspx");
                }
                else
                {
                    ScriptHelper.Alert("修改失败，请联系管理员。", this.Page);
                    return;
                }
            }

        }

    }
}