﻿$(function () {
    $("#AdminRegSubmit").click(function () {

        if ($("#AddAdminName").val().length == 0) {
            $("#AddAdminName").tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#AddAdminName").parent().removeClass("has-success").addClass("has-error");
            return false;
        }
        else {
            $("#AddAdminName").tooltip("hide");
            $("#AddAdminName").parent().addClass("has-success");
        }

        if ($("#AddAdminPsd").val().length == 0) {
            $("#AddAdminPsd").tooltip({ placement: 'right', trigger: 'manual', title: '信息未填写！' }).tooltip("show");
            $("#AddAdminPsd").parent().removeClass("has-success").addClass("has-error");
            return false;
        }
        else {
            $("#AddAdminPsd").tooltip("hide")
            $("#AddAdminPsd").parent().addClass("has-success");
        }

        return true;
    })

    $("#AdminregCancel").click(function () {
        history.back();
    })
})