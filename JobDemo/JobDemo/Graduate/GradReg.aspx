﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GradReg.aspx.cs" Inherits="JobDemo.Graduate.GradReg" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>毕业生注册</title>
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <link href="../DatePicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="../Style/Graduate.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-switch.min.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../DatePicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../DatePicker/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="../Scripts/Graduate/Graduate.js"></script>
    <script src="../BootStrap/js/bootstrap-switch.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <span class="navbar-brand">毕业生注册</span>
        </div>
    </nav>
    <form id="GrudRegisForm" runat="server" role="form" class="GradForm form-horizontal">
        <div class="form-group">
            <label for="AddGradName" class="col-sm-3 control-label">姓名：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddGradName" placeholder="姓名" runat="server" maxlength="16" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddGrudName" class="col-sm-3 control-label">用户名：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddGradUsrName" placeholder="用户名" runat="server" maxlength="20" />
            </div>
        </div>
        <div class="form-group">
            <label for="SexSwitch" class="col-sm-3 control-label">性别：</label>
            <div class="col-sm-7 switch">
                <input id="SexSwitch" type="checkbox" runat="server" />
                <input type="checkbox" id="AddGradSex" style="visibility: hidden;" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddGradSchool" class="col-sm-3 control-label">毕业院校：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddGradSchool" placeholder="毕业院校" runat="server" maxlength="50" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddGrudMajor" class="col-sm-3 control-label">专业限定：</label>
            <div class="col-sm-7">
                <select class="form-control" id="MajorSort" runat="server">
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="AddGrudSite" class="col-sm-3 control-label">当前所在地：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddGrudSite" placeholder="当前所在地" runat="server" maxlength="50" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddGrudTime" class="col-sm-3 control-label">毕业时间：</label>
            <div class="col-sm-3 input-append input-group" id="TimeGroup">
                <input type="text" value="" id="AddGrudTime" data-date-format="yyyy-mm-dd" placeholder="毕业时间" runat="server" class="form-control TimeGroupBtn" readonly size="9" />
                <span id="ShowBtnTime" class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                <span id="RemoveBtnTime" class="input-group-addon "><span class="glyphicon glyphicon-remove"></span></span>
            </div>
        </div>
        <div class="form-group">
            <label for="AddGrudBirthday" class="col-sm-3 control-label">生日：</label>
            <div class="col-sm-3 input-append input-group">
                <input id="AddGrudBirthday" type="text" value="" data-date-format="yyyy-mm-dd" placeholder="生日" runat="server" class="form-control TimeGroupBtn" readonly size="9" />
                <span id="ShowBtnBirthday" class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                <span id="RemoveBtnBirthday" class="input-group-addon "><span class="glyphicon glyphicon-remove"></span></span>
            </div>
        </div>
        <div class="form-group">
            <label for="AddGrudPsd" class="col-sm-3 control-label">密码：</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" id="AddGrudPsd" placeholder="密码" runat="server" maxlength="20" />
            </div>
        </div>

        <div class="form-group">
            <label for="AddGrudRePsd" class="col-sm-3 control-label">重复密码：</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" id="AddGrudRePsd" placeholder="重复密码" runat="server" maxlength="20" />
            </div>
        </div>

        <div class="GrudRegBtn form-group">
            <a id="GradRegBtn" class="btn btn-success" runat="server" onserverclick="GrudRegSubmitEvent">
                <span class="glyphicon glyphicon-ok"></span>提交
            </a>
            <a class="btn btn-danger" id="GrudregCancel">
                <span class="glyphicon glyphicon-remove"></span>取消
            </a>
        </div>

        <%--        Modal part--%>
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
            style="display: none;">
            <div class="modal-dialog" id="kkk">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">注册账号，开启明天</h4>
                    </div>
                    <div class="modal-body">
                        <div class="jumbotron">
                            <h1>"注册尚未成功，确认退出？"</h1>
                            <p>——&nbsp;&nbsp;开始事业第一步，就是现在。</p>
                        </div>
                        <button type="button" class="btn btn-success" data-dismiss="modal">取消退出</button>
                        <button id="TrueCancelBtn" type="button" class="btn btn-danger" data-dismiss="modal">确认退出</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
