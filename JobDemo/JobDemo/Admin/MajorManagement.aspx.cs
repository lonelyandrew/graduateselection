﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Tools;

namespace JobDemo.Admin
{
    public partial class MajorManagement : System.Web.UI.Page
    {

        private static int PageSize = 7;

        private static PagedDataSource DataSource;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Id"] == null || Session["Role"] == null)
            {
                Response.Redirect("../Default.aspx");
            }
            else if (Session["Role"].ToString() != "Admin")
            {
                Response.Redirect("../Default.aspx");
            }
            if (!IsPostBack)
            {
                MajorListBind();
                SetProfile();
            }
        }

        private void SetProfile()
        {
            DataRow Row = null;
            if (Session["Role"].ToString() == "Admin")
            {
                Model.Admin model = new Model.Admin();
                model.Id = Int32.Parse(Session["Id"].ToString());
                Row = BLLAdmin.GetInfoById(model);
                if (Row == null)
                {
                    Response.Redirect("../Default.aspx");
                }

                MyProfile.InnerHtml = "用户名：" + Row["UserName"] + "&nbsp;&nbsp;登录角色：管理员";
            }
            else
            {
                Response.Redirect("../Default.aspx");
            }
        }

        protected void LogOut(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("../Default.aspx");
        }

        public void MajorListBind()
        {
            DataSource = new PagedDataSource();
            DataSource.AllowPaging = true;
            DataSource.PageSize = PageSize;
            DataSource.CurrentPageIndex = 0;
            DataSource.DataSource = BLLMajor.GetAllMajorInfo().DefaultView;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        protected void LastPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex = DataSource.PageCount - 1;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        protected void FirstPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex = 0;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        protected void NextPage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex += 1;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        protected void PrePage(object sender, EventArgs e)
        {
            DataSource.CurrentPageIndex -= 1;
            DataListMajor.DataSource = DataSource;
            DataListMajor.DataBind();
            PagerButtonCheck();
        }

        private void PagerButtonCheck()
        {
            if (DataSource.CurrentPageIndex == (DataSource.PageCount - 1))
            {
                NextPageBtn.Disabled = true;
                LastPageBtn.Disabled = true;
            }
            if (DataSource.CurrentPageIndex == 0)
            {
                PrePageBtn.Disabled = true;
                FirstPageBtn.Disabled = true;

            }
            if (DataSource.CurrentPageIndex > 0)
            {
                PrePageBtn.Disabled = false;
                FirstPageBtn.Disabled = false;
            }
            if (DataSource.CurrentPageIndex < (DataSource.PageCount - 1))
            {
                NextPageBtn.Disabled = false;
                LastPageBtn.Disabled = false;
            }

            CurrentPageNum.InnerText = (DataSource.CurrentPageIndex + 1).ToString();
            TotalPageNum.InnerText = DataSource.PageCount.ToString();
        }

        protected void DelMajorClick(object sender, EventArgs e)
        {
            HtmlAnchor Anchor = (HtmlAnchor)sender;
            Major Model = new Major();
            Model.Id = Int32.Parse(Anchor.Attributes["value"].ToString());

            if (BLLMajor.DelMajor(Model))
            {
                ScriptHelper.Alert("删除成功。", this.Page);
            }
            else
            {
                ScriptHelper.Alert("删除失败。", this.Page);
            }
            MajorListBind();
        }

        protected void EditMajorClick(object sender, EventArgs e)
        {
            HtmlAnchor Anchor = (HtmlAnchor)sender;
            Major Model = new Major();
            Model.Id = Int32.Parse(Anchor.Attributes["value"].ToString());
            IdHidden.Value = Model.Id.ToString();
            AddMajorName.Value = BLLMajor.GetSingleMajorById(Model);
            MajorMultiView.ActiveViewIndex = 1;
        }

        protected void EditMajorSubmit(object sender, EventArgs e)
        {
            if (IdHidden.Value != "")
            {
                UpdateMajor();
            }
            else
            {
                CreateMajor();
            }
        }

        protected void CancelMajorEditClick(object sender, EventArgs e)
        {
            Response.Redirect("MajorManagement.aspx");
        }

        protected void CreateBtnClick(object sender, EventArgs e)
        {
            MajorMultiView.ActiveViewIndex = 1;
            AddMajorName.Value = "";
            IdHidden.Value = "";
        }

        protected void MultiDelMajorClick(object sender, EventArgs e)
        {
            string[] MajorIds = Request["MajorId"].ToString().Split(',');
            if (BLLMajor.MultiDelMajor(MajorIds))
            {
                ScriptHelper.Alert("删除成功。", this.Page);
                MajorListBind();
            }
            else
            {
                ScriptHelper.Alert("删除失败。", this.Page);
                MajorListBind();
            }
        }

        private void UpdateMajor()
        {
            if (BLLMajor.MajorNameIsValied(AddMajorName.Value) == Model.Major.MajorNameValid.Valid)
            {
                if (BLLMajor.ContainsMajor(AddMajorName.Value))
                {
                    ScriptHelper.Alert("已存在专业名称。", this.Page);
                    return;
                }
                Major model = new Major();
                model.Id = Int32.Parse(IdHidden.Value);
                model.Name = AddMajorName.Value;
                if (BLLMajor.UpdateMajor(model))
                {
                    Response.Redirect("MajorManagement.aspx");
                }
                else
                {
                    ScriptHelper.Alert("更新失败。", this.Page);
                }
            }
            else if (BLLMajor.MajorNameIsValied(AddMajorName.Value) == Model.Major.MajorNameValid.OutofMaxLength)
            {
                ScriptHelper.Alert("专业名称最大长度为20个字符。", this.Page);
                return;
            }
            else if (BLLMajor.MajorNameIsValied(AddMajorName.Value) == Model.Major.MajorNameValid.Empty)
            {
                ScriptHelper.Alert("专业名称不得为空。", this.Page);
                return;
            }
        }

        public void CreateMajor()
        {
            if (BLLMajor.MajorNameIsValied(AddMajorName.Value) == Model.Major.MajorNameValid.Valid)
            {
                if (BLLMajor.ContainsMajor(AddMajorName.Value))
                {
                    ScriptHelper.Alert("已存在专业名称，不得添加。", this.Page);
                    return;
                }
                Major model = new Major();
                model.Name = AddMajorName.Value;
                if (BLLMajor.AddMajor(model))
                {
                    Response.Redirect("MajorManagement.aspx");
                }
                else
                {
                    ScriptHelper.Alert("添加失败。", this.Page);
                }
            }
            else if (BLLMajor.MajorNameIsValied(AddMajorName.Value) == Model.Major.MajorNameValid.OutofMaxLength)
            {
                ScriptHelper.Alert("专业名称最大长度为20个字符。", this.Page);
                return;
            }
            else if (BLLMajor.MajorNameIsValied(AddMajorName.Value) == Model.Major.MajorNameValid.Empty)
            {
                ScriptHelper.Alert("专业名称不得为空。", this.Page);
                return;
            }
        }
    }
}