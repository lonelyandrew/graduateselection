﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminReg.aspx.cs" Inherits="JobDemo.Admin.AdminReg" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>管理员注册</title>
    <link href="../Style/Admin.css" rel="stylesheet" />
    <link href="../BootStrap/css/bootstrap-front.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../BootStrap/js/bootstrap.min.js"></script>
    <script src="../Scripts/Admin/AdminReg.js"></script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <span class="navbar-brand">管理员注册</span>
        </div>
    </nav>
    <form id="AdminRegisForm" runat="server" role="form" class="AdminForm form-horizontal adminform">
        <div class="form-group">
            <label for="AddAdminName" class="col-sm-3 control-label">用户名：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddAdminName" placeholder="用户名" runat="server" />
            </div>
        </div>
        <div class="form-group">
            <label for="AddAdminName" class="col-sm-3 control-label">密码：</label>
            <div class="col-sm-7">
                <input type="text" class="form-control" id="AddAdminPsd" placeholder="密码" runat="server" />
            </div>
        </div>
        <div class="AdminRegBtn form-group">
            <a class="btn btn-success Adminregbtn" runat="server" id="AdminRegSubmit" onserverclick="AdminRegSubmitEvent">
                <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;提交
            </a>
            <a class="btn btn-danger Adminregbtn" runat="server" id="AdminregCancel">
                <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;&nbsp;取消
            </a>
        </div>

    </form>
</body>
</html>
