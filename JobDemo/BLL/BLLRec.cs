﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class BLLRec
    {
        public static bool AddRec(Recruitment Model)
        {
            using (DALRec DALObj = new DALRec())
            {
                DALObj.Model = Model;
                return DALObj.Add();
            }
        }

        public static DataTable GetAllInfo()
        {
            using (DALRec DALObj = new DALRec())
            {
                return DALObj.GetList("");
            }
        }

        public static bool DelRec(Recruitment Model)
        {
            using (DALRec DALObj = new DALRec())
            {
                DALObj.Model = Model;
                return DALObj.Delete();
            }
        }

        public static DataRow GetSingleRecById(Recruitment Model)
        {
            using (DALRec DALObj = new DALRec())
            {
                DALObj.Model = Model;
                DataTable Table = DALObj.GetEdit(" Id = " + Model.Id);
                if (Table.Rows.Count > 0)
                {
                    return Table.Rows[0];
                }
                else
                {
                    return null;
                }
            }
        }

        public static bool UpdateRec(Recruitment Model)
        {
            using (DALRec DALObj = new DALRec())
            {
                DALObj.Model = Model;
                return DALObj.Update();
            }
        }

        public static DataTable GetRecByCompId(Recruitment Model)
        {
            using (DALRec DALObj = new DALRec())
            {
                DALObj.Model = Model;
                return DALObj.GetList(" CompanyId = " + Model.CompanyId);
            }
        }

        public static DataTable GetNeedList(string Message)
        {
            using (DALRec recruitment = new DALRec())
            {
                return recruitment.GetNeedList("CompanyName LIKE N'%" + Message + "%' OR Place LIKE N'%" + Message + "%' OR  Position LIKE N'%" + Message + "%' OR RecruitmentName LIKE N'%" + Message + "%'");
            }
        }

        public static DataTable GetNeedList(string Message, int MajorId)
        {
            using (DALRec recruitment = new DALRec())
            {
                return recruitment.GetNeedList("CompanyName LIKE N'%" + Message + "%' AND mid= " + MajorId.ToString() + " OR Place LIKE N'%" + Message + "%' OR  Position LIKE N'%" + Message + "%' OR RecruitmentName LIKE N'%" + Message + "%'");
            }
        }

        public static DataTable GetDetail(int Id)
        {
            using (DALRec DALObj = new DALRec())
            {
                DataTable Table = DALObj.GetList(" a.Id = " + Id.ToString());

                if (Table.Rows.Count == 0)
                {
                    Table = null;
                }

                return Table;
            }
        }

        public static DataTable GetTop5PeopleCountList()
        {
            using (DALRec recruitment = new DALRec())
            {
                return recruitment.GetTop5PeopleCountList();
            }
        }

        public static DataTable GetBottom3PeopleCountList()
        {
            using (DALRec recruitment = new DALRec())
            {
                return recruitment.GetBottom3PeopleCountList();
            }
        }

        public static DataTable GetTop3PeopleCountList()
        {
            using (DALRec recruitment = new DALRec())
            {
                return recruitment.GetTop3PeopleCountList();
            }
        }

        public static DataTable GetYearSumPeopleCountList()
        {
            using (DALRec recruitment = new DALRec())
            {
                return recruitment.GetYearSumPeopleCountList();
            }
        }
    }
}
