﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Model;
using System.Data;
using System.Web;

namespace BLL
{
    public static class BLLMajor
    {
        public static bool AddMajor(Major Model)
        {
            using (DALMajor DALObj = new DALMajor())
            {
                DALObj.Model = Model;
                return DALObj.Add();
            }
        }

        public static DataTable GetAllMajorInfo()
        {
            using (DALMajor DALObj = new DALMajor())
            {
                return DALObj.GetList("");
            }
        }

        public static bool DelMajor(Major Model)
        {
            using (DALMajor DALObj = new DALMajor())
            {
                DALObj.Model = Model;
                return DALObj.Delete();
            }
        }

        public static Model.Major.MajorNameValid MajorNameIsValied(string Name)
        {
            if (String.IsNullOrWhiteSpace(Name.Trim()))
            {
                return Model.Major.MajorNameValid.Empty;
            }

            if (Name.Trim().Length > 20)
            {
                return Model.Major.MajorNameValid.OutofMaxLength;
            }

            else
            {
                return Model.Major.MajorNameValid.Valid;
            }
        }

        public static string GetSingleMajorById(Major Model)
        {
            using (DALMajor DALObj = new DALMajor())
            {
                DALObj.Model = Model;
                DataTable MajorTable = DALObj.GetList(" Id = " + Model.Id);
                if (MajorTable.Rows.Count > 0)
                {
                    return MajorTable.Rows[0]["Name"].ToString();
                }
                else
                {
                    return null;
                }
            }
        }

        public static bool UpdateMajor(Major Model)
        {
            using (DALMajor DALObj = new DALMajor())
            {
                DALObj.Model = Model;
                return DALObj.Update();
            }
        }

        public static bool MultiDelMajor(string[] MajorIds)
        {
            using (DALMajor DALObj = new DALMajor())
            {
                return DALObj.MultiDel(MajorIds);
            }
        }

        public static bool ContainsMajor(string MajorName)
        {
            using (DALMajor DALObj = new DALMajor())
            {
                int a = DALObj.GetList("Name = N'" + MajorName + "'").Rows.Count;
                if (a > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
